 /*_---------------------------------------------Clear screen-------------------------------------*/
CLEAR SCREEN;
set serveroutput on size 1000000;

/*_----------------------------------------------------------------------------------------------*/
/*_-----------------------Clear existing data----------------------------------------------------*/
/*_----------------------------------------------------------------------------------------------*/
DROP TRIGGER on_SALE_delete;
DROP TRIGGER on_SEANSE_delete;
DROP TRIGGER on_FILMY_delete;

DROP TRIGGER zwieksz_pojemnosc_sali;
DROP TRIGGER czy_zajete;

DROP TRIGGER mod_KLIENCI;
DROP TRIGGER mod_GATUNKI;
DROP TRIGGER mod_FILMY;
DROP TRIGGER mod_SALE;
DROP TRIGGER mod_ZAMOWIENIA;
DROP TRIGGER mod_SEANSE;
DROP TRIGGER mod_MIEJSCA;
DROP TRIGGER mod_CENY;
DROP TRIGGER mod_REZYSERZY;

DELETE FROM ZAMOWIENIA;
DROP TABLE ZAMOWIENIA;

DELETE FROM SEANSE;
DROP TABLE SEANSE;

DELETE FROM FILMY;
DROP TABLE FILMY;

DELETE FROM MIEJSCA;
DROP TABLE MIEJSCA;

DELETE FROM CENY;
DROP TABLE CENY;

DELETE FROM GATUNKI;
DROP TABLE GATUNKI;

DELETE FROM SALE;
DROP TABLE SALE;

DELETE FROM REZYSERZY;
DROP TABLE REZYSERZY;

DELETE FROM KLIENCI;
DROP TABLE KLIENCI;

DELETE FROM INFO;
DROP TABLE INFO;

DROP SEQUENCE KLIENCI_SEQ;
DROP SEQUENCE REZYSERZY_SEQ;
DROP SEQUENCE GATUNKI_SEQ;
DROP SEQUENCE CENY_SEQ;
DROP SEQUENCE SALE_SEQ;
DROP SEQUENCE MIEJSCA_SEQ;
DROP SEQUENCE ZAMOWIENIA_SEQ;
DROP SEQUENCE FILMY_SEQ;
DROP SEQUENCE SEANSE_SEQ;
DROP SEQUENCE INFO_SEQ;
/*_----------------------------------------------------------------------------------------------*/
/*_----------------------Create tables from scratch----------------------------------------------*/
/*_----------------------------------------------------------------------------------------------*/
CREATE TABLE KLIENCI (
    KLI_ID 			INTEGER NOT NULL,
    KLI_Imie 	        	VARCHAR(20) NOT NULL,
    KLI_Nazwisko                VARCHAR(40) NOT NULL,
    KLI_Numer_telefonu          NUMBER(11),
    KLI_Email                   VARCHAR(50) NOT NULL
);
ALTER TABLE KLIENCI 
ADD CONSTRAINT KLI_PrimaryKey PRIMARY KEY(KLI_ID);


CREATE TABLE REZYSERZY (
    REZ_ID                      INTEGER NOT NULL,
    REZ_Imie_1                  VARCHAR(40) NOT NULL,
    REZ_Imie_2                  VARCHAR(40),
    REZ_Nazwisko                VARCHAR(60) NOT NULL
);
ALTER TABLE REZYSERZY
ADD CONSTRAINT REZ_PrimaryKey PRIMARY KEY(REZ_ID);

CREATE TABLE GATUNKI (
    GAT_ID                      INTEGER NOT NULL,
    GAT_Nazwa                   VARCHAR(30)
);
ALTER TABLE GATUNKI
ADD CONSTRAINT GAT_PrimaryKey PRIMARY KEY(GAT_ID);

CREATE TABLE CENY (
    CEN_ID                      INTEGER NOT NULL,
    CEN_Normalny                FLOAT,
    CEN_Ulgowy                  FLOAT
);
ALTER TABLE CENY 
ADD CONSTRAINT CEN_PrimaryKey PRIMARY KEY(CEN_ID);

CREATE TABLE SALE (
    SAL_ID                      INTEGER NOT NULL,
    SAL_Numer_sali              INTEGER NOT NULL,
    SAL_Pojemnosc               INTEGER DEFAULT 0
);
ALTER TABLE SALE 
ADD CONSTRAINT SAL_PrimaryKey PRIMARY KEY(SAL_ID);

CREATE TABLE MIEJSCA (
    MIE_ID                      INTEGER NOT NULL,
    MIE_Rzad                    INTEGER NOT NULL,
    MIE_MIEJSCA                 INTEGER NOT NULL,
    SAL_ID                      INTEGER NOT NULL
);

ALTER TABLE MIEJSCA 
ADD CONSTRAINT MIE_PrimaryKey PRIMARY KEY(MIE_ID);

CREATE TABLE ZAMOWIENIA (
    ZAM_ID                      INTEGER NOT NULL,
    KLI_ID                      INTEGER NOT NULL,
    SEA_ID                      INTEGER NOT NULL,
    MIE_ID                      INTEGER NOT NULL,
    ZAM_Czy_ulgowy              NUMBER DEFAULT 0
);

ALTER TABLE ZAMOWIENIA 
ADD CONSTRAINT ZAM_PrimaryKey PRIMARY KEY(ZAM_ID);

ALTER TABLE ZAMOWIENIA
ADD CONSTRAINT bool_czy_ulgowy CHECK (ZAM_Czy_ulgowy = 0 OR ZAM_Czy_ulgowy = 1);

CREATE TABLE FILMY (
    FIL_ID                      INTEGER NOT NULL,
    FIL_Tytul                   VARCHAR(200) NOT NULL,
    FIL_Opis                    BLOB,
    FIL_Dlugosc                 INTEGER,
    GAT_ID                      INTEGER,
    REZ_ID                      INTEGER
);

ALTER TABLE FILMY 
ADD CONSTRAINT FIL_PrimaryKey PRIMARY KEY(FIL_ID);

CREATE TABLE SEANSE (
    SEA_ID                      INTEGER NOT NULL,
    SEA_Termin                  DATE NOT NULL,
    SEA_Czy_premiera            INTEGER DEFAULT 0 NOT NULL,
    SAL_ID                      INTEGER NOT NULL,
    FIL_ID                      INTEGER NOT NULL,
    CEN_ID                      INTEGER NOT NULL
);

ALTER TABLE SEANSE 
ADD CONSTRAINT SEA_PrimaryKey PRIMARY KEY(SEA_ID);

ALTER TABLE SEANSE 
ADD CONSTRAINT bool_czy_premiera CHECK (SEA_Czy_premiera= 0 OR SEA_Czy_premiera = 1);

CREATE TABLE INFO (
    INF_ID                      INTEGER NOT NULL,
    INF_TABLE                   VARCHAR(30) NOT NULL,
    INF_USER                    VARCHAR(50) NOT NULL,
    INF_DATE                    DATE,
    INF_OPERATION               VARCHAR(20)
);

/*_----------------------------------------------------------------------------------------------*/
/*_---------------------------Foreign  keys------------------------------------------------------*/
/*_----------------------------------------------------------------------------------------------*/
ALTER TABLE MIEJSCA
ADD CONSTRAINT fk_sal_mie 
FOREIGN KEY(SAL_ID)
REFERENCES SALE(SAL_ID);

ALTER TABLE ZAMOWIENIA 
ADD CONSTRAINT fk_kli_zam
FOREIGN KEY(KLI_ID)
REFERENCES KLIENCI(KLI_ID);

ALTER TABLE ZAMOWIENIA 
ADD CONSTRAINT fk_sea_zam
FOREIGN KEY(SEA_ID)
REFERENCES SEANSE(SEA_ID);

ALTER TABLE ZAMOWIENIA 
ADD CONSTRAINT fk_mie_zam
FOREIGN KEY(MIE_ID)
REFERENCES MIEJSCA(MIE_ID);

ALTER TABLE FILMY 
ADD CONSTRAINT fk_gat_fil
FOREIGN KEY(GAT_ID)
REFERENCES GATUNKI(GAT_ID);

ALTER TABLE FILMY 
ADD CONSTRAINT fk_rez_fil
FOREIGN KEY(REZ_ID)
REFERENCES REZYSERZY(REZ_ID);

ALTER TABLE SEANSE
ADD CONSTRAINT fk_fil_sea
FOREIGN KEY(FIL_ID)
REFERENCES FILMY(FIL_ID);

ALTER TABLE SEANSE
ADD CONSTRAINT fk_cen_sea
FOREIGN KEY(CEN_ID)
REFERENCES CENY(CEN_ID);

ALTER TABLE SEANSE
ADD CONSTRAINT fk_sal_sea
FOREIGN KEY(SAL_ID)
REFERENCES SALE(SAL_ID);

/*_----------------------------------------------------------------------------------------------*/
/*_-------------------Autoincrement Triggers-----------------------------------------------------*/
/*_----------------------------------------------------------------------------------------------*/
CREATE SEQUENCE KLIENCI_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_KLIENCI_id
    BEFORE INSERT ON KLIENCI 
    FOR EACH ROW
BEGIN
    IF :NEW.KLI_ID IS NULL THEN
        :NEW.KLI_ID := KLIENCI_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into KLIENCI');
END;
/

CREATE SEQUENCE REZYSERZY_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_REZYSERZY_id
    BEFORE INSERT ON REZYSERZY 
    FOR EACH ROW
BEGIN
    IF :NEW.REZ_ID IS NULL THEN
        :NEW.REZ_ID := REZYSERZY_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into REZYSERZY');
END;
/

CREATE SEQUENCE GATUNKI_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_GATUNKI_id
    BEFORE INSERT ON GATUNKI 
    FOR EACH ROW
BEGIN
    IF :NEW.GAT_ID IS NULL THEN
        :NEW.GAT_ID := GATUNKI_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into GATUNKI');
END;
/

CREATE SEQUENCE CENY_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_CENY_id
    BEFORE INSERT ON CENY 
    FOR EACH ROW
BEGIN
    IF :NEW.CEN_ID IS NULL THEN
        :NEW.CEN_ID := CENY_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into CENY');
END;
/

CREATE SEQUENCE SALE_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_SALE_id
    BEFORE INSERT ON SALE 
    FOR EACH ROW
BEGIN
    IF :NEW.SAL_ID IS NULL THEN
        :NEW.SAL_ID := SALE_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into SALE');
END;
/

CREATE SEQUENCE MIEJSCA_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_MIEJSCA_id
    BEFORE INSERT ON MIEJSCA
    FOR EACH ROW
BEGIN
    IF :NEW.MIE_ID IS NULL THEN
        :NEW.MIE_ID := MIEJSCA_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into MIEJSCA');
END;
/

CREATE SEQUENCE ZAMOWIENIA_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_ZAMOWIENIA_id
    BEFORE INSERT ON ZAMOWIENIA
    FOR EACH ROW
BEGIN
    IF :NEW.ZAM_ID IS NULL THEN
        :NEW.ZAM_ID := ZAMOWIENIA_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into ZAMOWIENIA');
END;
/

CREATE SEQUENCE FILMY_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_FILMY_id
    BEFORE INSERT ON FILMY 
    FOR EACH ROW
BEGIN
    IF :NEW.FIL_ID IS NULL THEN
        :NEW.FIL_ID := FILMY_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into FILMY');
END;
/

CREATE SEQUENCE SEANSE_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 1000;
CREATE OR REPLACE TRIGGER auto_increment_SEANSE_id
    BEFORE INSERT ON SEANSE 
    FOR EACH ROW
BEGIN
    IF :NEW.SEA_ID IS NULL THEN
        :NEW.SEA_ID := SEANSE_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into SEANSE');
END;
/

CREATE SEQUENCE INFO_SEQ
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999 MINVALUE 1
    CACHE 999999;
CREATE OR REPLACE TRIGGER auto_increment_info_id
    BEFORE INSERT ON INFO 
    FOR EACH ROW
BEGIN
    IF :NEW.INF_ID IS NULL THEN
        :NEW.INF_ID := INFO_SEQ.nextval;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Inserting stuff into info');
END;
/
/*_----------------------------------------------------------------------------------------------*/
/*_----------------------------On delete triggers------------------------------------------------*/
/*_----------------------------------------------------------------------------------------------*/
CREATE OR REPLACE TRIGGER on_SALE_delete
    BEFORE DELETE ON SALE
    FOR EACH ROW
DECLARE
    SALE_id INTEGER;
BEGIN
    SALE_id := :old.SAL_ID;
    DELETE FROM MIEJSCA
    WHERE SAL_ID = SALE_id;
    DBMS_OUTPUT.PUT_LINE(' Deleting stuff from SALE');
END;
/

CREATE OR REPLACE TRIGGER on_SEANSE_delete
    BEFORE DELETE ON SEANSE
    FOR EACH ROW
DECLARE
    SEANSE_id INTEGER;
BEGIN
    SEANSE_id := :old.SEA_ID;
    DELETE FROM ZAMOWIENIA
    WHERE SEA_ID = SEANSE_id;
    DBMS_OUTPUT.PUT_LINE(' Deleting stuff from SEANSE');
END;
/

CREATE OR REPLACE TRIGGER on_FILMY_delete
    BEFORE DELETE ON FILMY
    FOR EACH ROW
DECLARE
    FILMY_id INTEGER;
BEGIN
    FILMY_id := :old.FIL_ID;
    DELETE FROM ZAMOWIENIA
    WHERE SEA_ID = FILMY_id;
    DBMS_OUTPUT.PUT_LINE(' Deleting stuff from FILMY');
END;
/
/*_----------------------------------------------------------------------------------------------*/
/*_----------------------------On insert---------------------------------------------------------*/
/*_----------------------------------------------------------------------------------------------*/
CREATE OR REPLACE TRIGGER zwieksz_pojemnosc_sali 
    AFTER INSERT ON MIEJSCA 
    FOR EACH ROW
BEGIN
    UPDATE SALE
    SET SAL_Pojemnosc = SAL_Pojemnosc + 1
    WHERE SAL_ID = :new.SAL_ID;
END;
/

CREATE OR REPLACE TRIGGER czy_zajete 
    BEFORE INSERT ON ZAMOWIENIA 
    FOR EACH ROW
DECLARE
    num NUMBER;
BEGIN
    SELECT COUNT(*) INTO num FROM ZAMOWIENIA WHERE SEA_ID = :new.sea_id and MIE_ID = :new.mie_id;
    
    IF num != 0 THEN 
        NULL;
    END IF;
END;
/

/*_----------------------------------------------------------------------------------------------*/
/*_----------------------------Logs Triggers-----------------------------------------------------*/
/*_----------------------------------------------------------------------------------------------*/
CREATE OR REPLACE TRIGGER mod_KLIENCI
    AFTER INSERT OR UPDATE OR DELETE ON KLIENCI 
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'KLIENCI',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/

CREATE OR REPLACE TRIGGER mod_MIEJSCA
    AFTER INSERT OR UPDATE OR DELETE ON MIEJSCA 
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'MIEJSCA',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/

CREATE OR REPLACE TRIGGER mod_GATUNKI
    AFTER INSERT OR UPDATE OR DELETE ON GATUNKI
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'GATUNKI',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/

CREATE OR REPLACE TRIGGER mod_ZAMOWIENIA
    AFTER INSERT OR UPDATE OR DELETE ON ZAMOWIENIA 
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'ZAMOWIENIA',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/

CREATE OR REPLACE TRIGGER mod_FILMY
    AFTER INSERT OR UPDATE OR DELETE ON FILMY 
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'FILMY',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/


CREATE OR REPLACE TRIGGER mod_SEANSE
    AFTER INSERT OR UPDATE OR DELETE ON SEANSE 
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'SEANSE',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/

CREATE OR REPLACE TRIGGER mod_REZYSERZY
    AFTER INSERT OR UPDATE OR DELETE ON REZYSERZY 
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'REZYSERZY',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/

CREATE OR REPLACE TRIGGER mod_CENY
    AFTER INSERT OR UPDATE OR DELETE ON CENY 
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'CENY',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/

CREATE OR REPLACE TRIGGER mod_SALE
    AFTER INSERT OR UPDATE OR DELETE ON SALE 
    FOR EACH ROW
DECLARE
    op VARCHAR(20);
BEGIN
    IF INSERTING THEN 
        op := 'INSERT';
    ELSIF DELETING THEN 
        op := 'DELETE';
    ELSIF UPDATING THEN 
        op := 'UPDATE';
    END IF;
    INSERT INTO INFO (INF_ID, INF_TABLE, INF_USER, INF_DATE, INF_OPERATION) 
        VALUES(
            NULL,
            'SALE',
            (SELECT USER FROM dual),
            (SELECT SYSDATE FROM dual),
            (op)
        );
END;
/

/*_----------------------------------------------------------------------------------------------*/
/*_----------------------------Some views--------------------------------------------------------*/
/*_----------------------------------------------------------------------------------------------*/
CREATE OR REPLACE VIEW PREMIERA AS
    SELECT S.SEA_ID, S.SEA_Termin, P.SAL_Numer_sali, F.FIL_Tytul, C.CEN_Normalny, C.CEN_Ulgowy 
    FROM SEANSE S
    INNER JOIN FILMY F
        ON S.FIL_ID = F.FIL_ID
    INNER JOIN CENY C
        ON S.CEN_ID = C.CEN_ID
    INNER JOIN SALE P
        ON S.SAL_ID = P.SAL_ID
    WHERE S.SEA_Czy_premiera = 1;

CREATE OR REPLACE VIEW NORM_SEANSE AS
    SELECT S.SEA_ID, S.SEA_Termin, P.SAL_Numer_sali, F.FIL_Tytul, C.CEN_Normalny, C.CEN_Ulgowy 
    FROM SEANSE S
    INNER JOIN FILMY F
        ON S.FIL_ID = F.FIL_ID
    INNER JOIN CENY C
        ON S.CEN_ID = C.CEN_ID
    INNER JOIN SALE P
        ON S.SAL_ID = P.SAL_ID
    WHERE S.SEA_Czy_premiera = 0;


CREATE OR REPLACE VIEW REZERWACJE_ULGOWE AS
    SELECT KLI_Imie || ' ' || KLI_Nazwisko AS Imie_i_nazwisko, KLI_Email, SEA_Termin, FIL_Tytul, MIE_Rzad, MIE_MIEJSCA
    FROM ZAMOWIENIA Z
    INNER JOIN SEANSE S
        ON S.SEA_ID = Z.SEA_ID
    INNER JOIN FILMY F
        ON S.FIL_ID = F.FIL_ID
    INNER JOIN KLIENCI K
        ON Z.KLI_ID = K.KLI_ID
    INNER JOIN MIEJSCA M
        ON Z.MIE_ID = M.MIE_ID
    WHERE Z.ZAM_Czy_ulgowy = 1;

CREATE OR REPLACE VIEW REZERWACJE_NORMALNE AS
    SELECT KLI_Imie || ' ' || KLI_Nazwisko AS Imie_i_nazwisko, KLI_Email, SEA_Termin, FIL_Tytul, MIE_Rzad, MIE_MIEJSCA
    FROM ZAMOWIENIA Z
    INNER JOIN SEANSE S
        ON S.SEA_ID = Z.SEA_ID
    INNER JOIN FILMY F
        ON S.FIL_ID = F.FIL_ID
    INNER JOIN KLIENCI K
        ON Z.KLI_ID = K.KLI_ID
    INNER JOIN MIEJSCA M
        ON Z.MIE_ID = M.MIE_ID
    WHERE Z.ZAM_Czy_ulgowy = 0;

CREATE OR REPLACE VIEW INFO_INSERTS AS
    SELECT INF_TABLE, INF_USER, INF_DATE, INF_OPERATION
    FROM INFO
    WHERE INF_OPERATION = 'INSERT';

CREATE OR REPLACE VIEW INFO_UPDATES AS
    SELECT INF_TABLE, INF_USER, INF_DATE, INF_OPERATION
    FROM INFO
    WHERE INF_OPERATION = 'UPDATES';

CREATE OR REPLACE VIEW INFO_DELETES AS
    SELECT INF_TABLE, INF_USER, INF_DATE, INF_OPERATION
    FROM INFO
    WHERE INF_OPERATION = 'DELETES';

/*-----------------------------------------------------------------------------------------------*/
/*-----------------------------Insert procedures-------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE INS_FILM_GAT_REZ(
    imie1 in VARCHAR2, 
    imie2 in VARCHAR2, 
    nazwisko IN VARCHAR2,
    tytul in VARCHAR2, 
    dlugosc in INTEGER, 
    gat in VARCHAR2 
)
IS
    rez_curr_id REZYSERZY.REZ_ID%TYPE;
    gat_curr_id GATUNKI.GAT_ID%TYPE;
BEGIN
    INSERT INTO GATUNKI(GAT_ID, GAT_Nazwa)
    VALUES (null, gat);
        
    INSERT INTO REZYSERZY(REZ_ID, REZ_Imie_1, REZ_Imie_2, REZ_Nazwisko) 
    VALUES (null, imie1, imie2, nazwisko);

    SELECT GATUNKI_SEQ.currval INTO gat_curr_id from dual;
    SELECT REZYSERZY_SEQ.currval INTO rez_curr_id from dual;
    INSERT INTO FILMY(FIL_ID, FIL_Tytul, FIL_Opis, FIL_Dlugosc, GAT_ID, REZ_ID) 
    VALUES(null, tytul, null, dlugosc, gat_curr_id, rez_curr_id);
END;
/

CREATE OR REPLACE PROCEDURE INS_SEA_FIL(
    termin in DATE,
    czy_premiera in INTEGER,
    cena_id in INTEGER,
    sala_id in INTEGER, 
    imie1 in VARCHAR2, 
    imie2 in VARCHAR2, 
    nazwisko IN VARCHAR2,
    tytul in VARCHAR2, 
    dlugosc in INTEGER, 
    gat in VARCHAR2 
)
IS
    rez_curr_id REZYSERZY.REZ_ID%TYPE;
    gat_curr_id GATUNKI.GAT_ID%TYPE;
    fil_curr_id FILMY.FIL_ID%TYPE;
BEGIN
    INSERT INTO GATUNKI(GAT_ID, GAT_Nazwa)
    VALUES (null, gat);
        
    INSERT INTO REZYSERZY(REZ_ID, REZ_Imie_1, REZ_Imie_2, REZ_Nazwisko) 
    VALUES (null, imie1, imie2, nazwisko);

    SELECT GATUNKI_SEQ.currval INTO gat_curr_id from dual;
    SELECT REZYSERZY_SEQ.currval INTO rez_curr_id from dual;
    INSERT INTO FILMY(FIL_ID, FIL_Tytul, FIL_Opis, FIL_Dlugosc, GAT_ID, REZ_ID) 
    VALUES(null, tytul, null, dlugosc, gat_curr_id, rez_curr_id);

    SELECT FILMY_SEQ.currval INTO fil_curr_id from dual;
    INSERT INTO SEANSE(
        SEA_ID, 
        SEA_TERMIN,
        SEA_Czy_premiera,
        SAL_ID,
        FIL_ID,
        CEN_ID 
    )
    VALUES(
        null,
        termin,
        czy_premiera,
        sala_id,
        fil_curr_id,
        cena_id
    );
END;
/

CREATE OR REPLACE PROCEDURE change_klient_mail(
    domena_przed    IN VARCHAR2,
    domena_po       IN VARCHAR2
)
IS
    CURSOR c1 IS 
        SELECT * 
        FROM KLIENCI
        WHERE REGEXP_LIKE(KLI_Email, '[A-Za-z0-9._%+-]+@' || domena_przed)
        FOR UPDATE;

    klient c1%ROWTYPE;
BEGIN
    OPEN c1;
    LOOP
        FETCH c1 INTO klient; 
        EXIT WHEN c1%NOTFOUND;

        UPDATE KLIENCI 
        SET KLI_Email = REGEXP_SUBSTR(klient.KLI_Email, '[A-Za-z0-9._%+-]+@') || domena_po
        WHERE CURRENT OF c1;
    END LOOP;
    CLOSE c1;
END;
/


CREATE OR REPLACE PROCEDURE change_seans_date(
    data_przed_str  IN VARCHAR2,
    data_po_str     IN VARCHAR2
)
IS
    data_przed  SEANSE.SEA_Termin%TYPE;
    data_po     SEANSE.SEA_TERMIN%TYPE;
BEGIN
    SELECT TO_DATE(data_przed_str, 'dd/mm/yyyy') 
        INTO data_przed 
        FROM DUAL;
    SELECT TO_DATE(data_po_str, 'dd/mm/yyyy') 
        INTO data_po
        FROM DUAL;

    UPDATE SEANSE
        SET SEA_Termin = data_po
        WHERE SEA_Termin = data_przed;
    IF SQL%FOUND THEN
        dbms_output.put_line('Updated');
    END IF;

    IF SQL%NOTFOUND THEN
        dbms_output.put_line('NOT Updated - NO records Found');
    END IF; 

    IF SQL%ROWCOUNT>0 THEN
        dbms_output.put_line(SQL%ROWCOUNT||' Rows Updated');
    ELSE
        dbms_output.put_line('NO Rows Updated Found');
    END IF;
END;
/

DECLARE
    TYPE date_rec 
    IS RECORD (
        data_przed  SEANSE.SEA_Termin%TYPE,
        data_po     SEANSE.SEA_Termin%TYPE
    );

    TYPE date_str_rec 
    IS RECORD (
        data_przed  VARCHAR2(10),
        data_po     VARCHAR2(10)
    );
PROCEDURE change_seans_date_rec(data_str date_str_rec)
    IS
       data         date_rec; 
    BEGIN
        SELECT 
            TO_DATE(data_str.data_przed, 'dd/mm/yyyy'), 
            TO_DATE(data_str.data_po, 'dd/mm/yyyy')
        INTO
            data.data_przed,
            data.data_po
        FROM
            DUAL;

        UPDATE SEANSE
            SET SEA_Termin = data.data_po
            WHERE SEA_Termin = data.data_przed;
    END change_seans_date_rec;
BEGIN
    NULL;
END;
/

DECLARE
    TYPE SEANS_TYPE
    IS RECORD (
        termin          SEANS.SEA_Termin%TYPE,
        sala            SALE.SAL_Numer_sali%TYPE, 
    );
PROCEDURE edit_seans_termin_sala(seans_przed, seans_po)
    IS
        sala_przed_temp       SALE%ROWTYPE;
        sala_po_temp       SALE%ROWTYPE;
    BEGIN
        SELECT * FROM SALE
        INTO sala_przed_temp
        WHERE SAL_Numer_sali = seans_przed.sala;

        SELECT * FROM SALE
        INTO sala_po_temp
        WHERE SAL_Numer_sali = seans_po.sala;

        UPDATE SEANSE
        SET SEA_Termin = seans_po.termin,
            SAL_ID = sala_po_temp.sal_id
        WHERE SEA_Termin = seans_przed.termin
        AND   SAL_ID = seans_przed_temp.sal_id;
    END edit_seans_termin_sala;
BEGIN
    NULL;
END;
/


