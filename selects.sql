CLEAR SCREEN;

PROMPT 'Nazwy gatunków z id <1,20> i id > 97';
SELECT GAT_Nazwa FROM GATUNKI
WHERE GAT_ID BETWEEN 1 AND 20
        OR GAT_ID > 97;

PROMPT 'Tyt. filmow ktore naleza do gatunkow z id < 8 + nazwy tych gatunkow';
COLUMN FIL_TYTUL HEADING 'Tytul' FORMAT A12 
COLUMN GAT_NAZWA HEADING 'Nazwa gatunku' FORMAT A20
SELECT F.FIL_Tytul, G.GAT_Nazwa FROM FILMY F
    INNER JOIN GATUNKI G
    ON F.GAT_ID = G.GAT_ID
WHERE F.GAT_ID < 8;

PROMPT 'Imie i nazwisko + email klientow z id < 10';
COLUMN IMIE_I_NAZWISKO HEADING 'Imie i nazwisko klienta' FORMAT A25
COLUMN KLI_EMAIL HEADING 'Email' FORMAT A20
SELECT KLI_Imie || ' ' || KLI_Nazwisko as Imie_i_Nazwisko, KLI_Email FROM KLIENCI
WHERE KLI_ID < 10;

PROMPT 'ID, Cena normalnego i cena ulgowego, gdzie ulgowy > normalny');
COLUMN CEN_ID HEADING 'ID' FORMAT 999 
COLUMN CEN_NORMALNY HEADING 'Normalny' FORMAT 99.99 
COLUMN CEN_ULGOWY HEADING 'Ulgowy' FORMAT 99.99 
COLUMN ROZNICA HEADING 'Roznica' Format 99.99
SELECT CEN_ID, CEN_Normalny, CEN_Ulgowy, CEN_Ulgowy - CEN_Normalny as ROZNICA FROM CENY
WHERE CEN_Ulgowy > CEN_Normalny;

PROMPT 'Ceny gdzie cena normalnego > 2 * cena ulgowego';
SELECT CEN_ID, CEN_Normalny, CEN_Ulgowy,CEN_Normalny - CEN_Ulgowy as ROZNICA FROM CENY
WHERE CEN_Ulgowy < CEN_Normalny / 2;

PROMPT 'Srednia z cen';
COLUMN AVG_Normalny HEADING 'Sr. norm.' FORMAT 99.99
COLUMN AVG_Ulgowy HEADING 'Sr. ulg.' FORMAT 99.99
SELECT AVG(CEN_Normalny) AS AVG_Normalny, AVG(CEN_Ulgowy) AS AVG_Ulgowy FROM CENY;

PROMPT 'Sale o pojemnosci > 300 os.';
COLUMN SAL_Numer_sali HEADING 'Nr sali' FORMAT 999 
COLUMN SAL_Pojemnosc HEADING 'Pojemnosc' FORMAT 99999 
SELECT SAL_Numer_sali, SAL_Pojemnosc FROM SALE
WHERE SAL_Pojemnosc > 300;

PROMPT 'Seanse premierowe';
COLUMN SEA_ID HEADING 'ID' FORMAT 999 
COLUMN SEA_Termin HEADING 'Termin'
SELECT * FROM PREMIERA;

PROMPT 'Seanse normalne';
SELECT * FROM NORM_SEANSE;

PROMPT 'Rezerwacje ulgowe';
COLUMN MIE_Miejsca HEADING 'Msc' FORMAT 99 
COLUMN MIE_Rzad HEADING 'Rzd' FORMAT 999 
SELECT * FROM REZERWACJE_ULGOWE;

PROMPT 'Rezerwacje normalne';
SELECT * FROM REZERWACJE_NORMALNE;

PROMPT '10 pierwszych logow';
COLUMN INF_TABLE HEADING 'Table' FORMAT A12
COLUMN INF_USER HEADING 'User' FORMAT A18
COLUMN INF_OPERATION HEADING 'OPERATION' FORMAT A10
SELECT * FROM INFO_INSERTS WHERE ROWNUM <= 10;

SELECT COUNT(*) AS Liczba, 'onet.pl' as DOMENA FROM KLIENCI
WHERE REGEXP_LIKE(KLI_Email, '*@onet.pl');

SELECT COUNT(*) AS Liczba, 'gmail.com' as DOMENA FROM KLIENCI
WHERE REGEXP_LIKE(KLI_Email, '*@gmail.com');

SELECT COUNT(*) AS Liczba, 'interia.pl' as DOMENA FROM KLIENCI
WHERE REGEXP_LIKE(KLI_Email, '*@interia.pl');
