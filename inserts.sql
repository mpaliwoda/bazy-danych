CREATE OR REPLACE 
    TYPE temp_arr IS VARRAY(5000)
    OF VARCHAR2(1000);
/
    
CREATE TABLE temp (
    imiona      temp_arr,
    nazwiska    temp_arr,
    filmy       temp_arr,
    gatunki     temp_arr
);

INSERT INTO temp(imiona, nazwiska, filmy, gatunki) VALUES( 
    temp_arr(
        'Zuzanna',
        'Kornelia',
        'Jakub',
        'Oliwier',
        'Lena',
        'Magdalena',
        'Antoni',
        'Karol',
        'Julia',
        'Karolina',
        'Szymon',
        'Oskar',
        'Maja ',
        'Michalina',
        'Jan ',
        'Maciej',
        'Zofia',
        'Weronika',
        'Filip',
        'Tomasz',
        'Hanna ',
        'Marcelina',
        'Kacper',
        'Natan',
        'Amelia',
        'Agata',
        'Aleksander',
        'Dominik',
        'Aleksandra',
        'Jagoda',
        'Franciszek',
        'Krzysztof',
        'Alicja ',
        'Helena',
        'Mikołaj',
        'Tymon',
        'Natalia ',
        'Nina ',
        'Wojciech',
        'Fabian',
        'Wiktoria ',
        'Barbara ',
        'Adam',
        'Hubert',
        'Oliwia ',
        'Blanka',
        'Michał ',
        'Paweł',
        'Maria',
        'Andrzej'
    ),
    temp_arr(
        'Nowak',
        'Wójcik',
        'Kowalczyk',
        'Woźniak',
        'Kaczmarek',
        'Mazur',
        'Krawczyk',
        'Nowicki',
        'Adamczyk',
        'Dudek',
        'Zając',
        'Wieczorek',
        'Król',
        'Wróbel',
        'Pawlak',
        'Walczak',
        'Stępień',
        'Michalak',
        'Sikora',
        'Baran',
        'Duda',
        'Szewczyk',
        'Pietrzak',
        'Marciniak',
        'Zawadzki',
        'Bąk',
        'Włodarczyk',
        'Czarnecki',
        'Sawicki',
        'Kubiak',
        'Wilk',
        'Lis',
        'Mazurek',
        'Wysocki',
        'Kaźmierczak',
        'Sobczak',
        'Cieślak',
        'Głowacki',
        'Kołodziej',
        'Szymczak',
        'Szulc',
        'Nowacki',
        'Błaszczyk',
        'Chojnacki',
        'Mróz',
        'Szczepaniak',
        'Górecki',
        'Krupa',
        'Kaczmarczyk',
        'Urbaniak',
        'Kozak',
        'Kania',
        'Mikołajczyk',
        'Mucha',
        'Tomczak',
        'Kozioł',
        'Kowalik',
        'Nawrocki',
        'Janik',
        'Musiał',
        'Wawrzyniak',
        'Markiewicz',
        'Tomczyk',
        'Jarosz',
        'Kołodziejczyk',
        'Kurek',
        'Kopeć',
        'Żak',
        'Łuczak',
        'Dziedzic',
        'Kot',
        'Stasiak',
        'Stankiewicz',
        'Piątek',
        'Jóźwiak',
        'Urban',
        'Pawlik',
        'Kruk',
        'Domagała',
        'Piasecki',
        'Wierzbicki',
        'Polak',
        'Zięba',
        'Janicki',
        'Wójtowicz',
        'Bednarek',
        'Majchrzak',
        'Bielecki',
        'Małecki',
        'Maj',
        'Sowa',
        'Gajda',
        'Klimek',
        'Olejniczak',
        'Ratajczak',
        'Madej',
        'Kasprzak',
        'Grzelak',
        'Socha',
        'Czajka',
        'Marek',
        'Kowal',
        'Bednarczyk',
        'Wrona',
        'Owczarek',
        'Matusiak',
        'Olejnik',
        'Mazurkiewicz',
        'Pająk',
        'Czech',
        'Łukasik',
        'Leśniak',
        'Cieślik'
    ),
    temp_arr (
        'Obywatel Kane',
        'Casablanca',
        'Ojciec chrzestny',
        'Przeminęło z wiatrem',
        'Lawrence z Arabii',
        'Czarnoksiężnik z krainy Oz',
        'Absolwent',
        'Na nabrzeżach',
        'Lista Schindlera',
        'Deszczowa piosenka',
        'To wspaniałe życie',
        'Bulwar Zachodzącego Słońca',
        'Most na rzece Kwai',
        'Pół żartem, pół serio',
        'Gwiezdne wojny: część IV – Nowa nadzieja',
        'Wszystko o Ewie',
        'Afrykańska królowa',
        'Psychoza',
        'Generał',
        'Chinatown',
        'Lot nad kukułczym gniazdem',
        'Grona gniewu',
        '2001: Odyseja kosmiczna',
        'Sokół maltański',
        'Wściekły Byk',
        'E.T.',
        'Dr. Strangelove',
        'Bonnie i Clyde',
        'Czas Apokalipsy',
        'Pan Smith jedzie do Waszyngtonu',
        'Skarb Sierra Madre',
        'Annie Hall',
        'Ojciec chrzestny II',
        'W samo południe',
        'Zabić drozda',
        'Ich noce',
        'Nocny kowboj',
        'Najlepsze lata naszego życia',
        'Podwójne ubezpieczenie',
        'Doktor Żywago',
        'Północ, północny zachód',
        'West Side Story',
        'Okno na podwórze',
        'King Kong',
        'Narodziny narodu',
        'Tramwaj zwany pożądaniem',
        'Mechaniczna pomarańcza',
        'Taksówkarz',
        'Szczęki',
        'Królewna Śnieżka i siedmiu krasnoludków',
        'Nietolerancja',
        'Butch Cassidy i Sundance Kid',
        'Władca Pierścieni: Drużyna Pierścienia',
        'Filadelfijska opowieść',
        'Stąd do wieczności',
        'Amadeusz',
        'Na Zachodzie bez zmian',
        'Dźwięki muzyki',
        'MASH',
        'Trzeci człowiek',
        'Fantazja',
        'Buntownik bez powodu',
        'Nashville',
        'Poszukiwacze zaginionej Arki',
        'Zawrót głowy',
        'Podróże Sullivana',
        'Tootsie',
        'Dyliżans',
        'Kabaret',
        'Bliskie spotkania trzeciego stopnia',
        'Milczenie owiec',
        'Sieć',
        'Przeżyliśmy wojnę',
        'Kto się boi Virginii Woolf?',
        'Amerykanin w Paryżu',
        'Jeździec znikąd',
        'Francuski łącznik',
        'Forrest Gump',
        'Szeregowiec Ryan',
        'Ben-Hur',
        'Skazani na Shawshank',
        'Wichrowe Wzgórza',
        'Gorączka złota',
        'Tańczący z wilkami',
        'W upalną noc',
        'Światła wielkiego miasta',
        'Amerykańskie graffiti',
        'Wszyscy ludzie prezydenta',
        'Rocky',
        'Łowca jeleni',
        'Dzika banda',
        'Dzisiejsze czasy',
        'Spartakus',
        'Olbrzym',
        'Wschód słońca',
        'Pluton',
        'Titanic',
        'Fargo',
        'Kacza zupa',
        'Noc w operze',
        'Bunt na Bounty',
        'Frankenstein',
        'Dwunastu gniewnych ludzi',
        'Swobodny jeździec',
        'Patton',
        'Szósty zmysł',
        'Śpiewak jazzbandu',
        'Lekkoduch',
        'My Fair Lady',
        'Wybór Zofii',
        'Miejsce pod słońcem',
        'Garsoniera',
        'Chłopcy z ferajny',
        'Pulp Fiction',
        'Ostatni seans filmowy',
        'Poszukiwacze',
        'Rób, co należy',
        'Drapieżne maleństwo',
        'Łowca androidów',
        'Bez przebaczenia',
        'Zgadnij, kto przyjdzie na obiad',
        'Toy Story',
        'Yankee Doodle Dandy'
    ),
    temp_arr(
        'Horror',
        'Thriller',
        'Kryminał',
        'Dramat'
    )
);

Declare
    rows_inserted number := 1;
    temp_var      temp_arr;
Begin
    Loop
    Begin
        SELECT gatunki INTO temp_var FROM temp;

        INSERT INTO GATUNKI(GAT_ID, GAT_Nazwa)
        VALUES(
            null,
            temp_var(rows_inserted + 2)    
        );
        rows_inserted := rows_inserted + 2;
        Exception When DUP_VAL_ON_INDEX Then Null;
        End;
    exit when rows_inserted = temp_var.count;
    End loop;
End;
/

DECLARE
    rows_inserted   number := 1;
    domena          varchar3(255);
    temp_im         temp_arr;
    temp_naz        temp_arr;
Begin
    Loop
    Begin
        SELECT imiona INTO temp_im FROM temp;
        SELECT nazwiska INTO temp_naz FROM temp;
        
        domena := '@gmail.com';
        IF MOD(rows_inserted, 6) = 0 THEN domena := '@onet.pl';
        ELSIF MOD(rows_inserted, 5) = 0 THEN domena := '@interia.pl';
        END IF;
        INSERT INTO KLIENCI(
            KLI_ID, 
            KLI_Imie, 
            KLI_Nazwisko, 
            KLI_Numer_telefonu,
            KLI_Email
        )
        VALUES(
            null,
            temp_im(dbms_random.value(2, temp_im.count)),
            temp_naz(dbms_random.value(2, temp_naz.count)),
            dbms_random.value(10000000001, 99999999999),
            dbms_random.string('L', 6)||domena
        );
        rows_inserted := rows_inserted + 2;
        Exception When DUP_VAL_ON_INDEX Then Null;
        End;
    exit when rows_inserted = 101;
    End loop;
End;
/

Declare
    rows_inserted   number := 1;
    temp_im         temp_arr;
    temp_naz        temp_arr;
Begin
    Loop
    Begin
        SELECT imiona INTO temp_im FROM temp;
        SELECT nazwiska INTO temp_naz FROM temp;
        
        INSERT INTO REZYSERZY(
            REZ_ID, 
            REZ_Imie_2, 
            REZ_Imie_3, 
            REZ_Nazwisko
        )
        VALUES(
            null,
            temp_im(dbms_random.value(2, temp_im.count)),
            temp_im(dbms_random.value(2, temp_im.count)),
            temp_naz(dbms_random.value(2, temp_naz.count))
        );
        rows_inserted := rows_inserted + 2;
        Exception When DUP_VAL_ON_INDEX Then Null;
        End;
    exit when rows_inserted = 101;
    End loop;
End;
/

Declare
    rows_inserted number := 1;
Begin
    Loop
    Begin
        INSERT INTO CENY(
            CEN_ID, 
            CEN_Normalny,
            CEN_Ulgowy
        )
        VALUES(
            null,
            dbms_random.value(1.1, 50.0),
            dbms_random.value(51.1, 100.0)
        );
        rows_inserted := rows_inserted + 2;
        Exception When DUP_VAL_ON_INDEX Then Null;
        End;
    exit when rows_inserted = 21;
    End loop;
End;
/

Declare
    rows_inserted number := 0;
    max_nr number;
Begin
    SELECT MAX(SAL_Numer_sali) 
    INTO max_nr 
    FROM SALE;
   
    IF max_nr is NULL THEN max_nr := 0; END IF;

    Loop
    Begin
        INSERT INTO SALE(
            SAL_ID, 
            SAL_Numer_sali,
            SAL_Pojemnosc 
        )
        VALUES(
            null,
            max_nr + rows_inserted + 1,
            0 
        );
    rows_inserted := rows_inserted + 1;
    Exception When DUP_VAL_ON_INDEX Then Null;
    End;
    exit when rows_inserted = 3;
    End loop;
End;
/

Declare
    il_sal NUMBER;
    i_max NUMBER;
    j_max NUMBER;
Begin
    SELECT SALE_SEQ.currval 
    INTO il_sal
    FROM dual;
    FOR x in 1..il_sal LOOP
        i_max := DBMS_RANDOM.VALUE(10, 40);
        j_max := DBMS_RANDOM.VALUE(10, 40);

        FOR i IN 1..i_max LOOP
            FOR j IN 1..j_max LOOP
                INSERT INTO MIEJSCA(
                    MIE_ID, 
                    MIE_Rzad,
                    MIE_MIEJSCA,
                    SAL_ID
                )
                VALUES(
                    null,
                    i,
                    j,
                    x 
                );
            END LOOP;
        END LOOP;
    END LOOP;
End;
/

Declare
    rows_inserted   number := 0;
    il_gat          number := 0;
    il_rez          number := 0;
    temp_fil        temp_arr;
Begin
    SELECT GATUNKI_SEQ.currval 
    INTO il_gat
    FROM dual;

    SELECT REZYSERZY_SEQ.currval
    INTO il_rez
    FROM dual;

    SELECT filmy INTO temp_fil FROM temp;

    Loop
    Begin
        INSERT INTO FILMY(
            FIL_ID, 
            FIL_Tytul,
            FIL_Opis,
            FIL_Dlugosc,
            GAT_ID,
            REZ_ID
        )
        VALUES(
            null,
            temp_fil(rows_inserted + 1),
            null,
            null,
            dbms_random.value(1, il_gat),
            dbms_random.value(1, il_rez)
        );
        rows_inserted := rows_inserted + 1;
    Exception When DUP_VAL_ON_INDEX Then Null;
    End;
    exit when rows_inserted = temp_fil.count;
    End loop;
End;
/

Declare
    rows_inserted number := 0;
    il_fil number := 0;
    il_cen number := 0;
    il_sal number := 0;
Begin
    SELECT FILMY_SEQ.currval 
    INTO il_fil
    FROM dual;

    SELECT CENY_SEQ.currval 
    INTO il_cen
    FROM dual;

    SELECT SALE_SEQ.currval 
    INTO il_sal
    FROM dual;

    Loop
    Begin
        INSERT INTO seanse(
            SEA_ID, 
            SEA_TERMIN,
            SEA_Czy_premiera,
            SAL_ID,
            FIL_ID,
            CEN_ID 
        )
        VALUES(
            null,
            TO_DATE(TRUNC(DBMS_RANDOM.VALUE (2451545, 5373484)), 'J'),
            MOD(rows_inserted, 2),
            dbms_random.value(1, il_sal),
            dbms_random.value(1, il_fil),
            dbms_random.value(1, il_cen)
        );
        rows_inserted := rows_inserted + 1;
    Exception When DUP_VAL_ON_INDEX Then Null;
    End;
    exit when rows_inserted = 50;
    End loop;
End;
/

Declare
    rows_inserted number := 0;
    il_kli number := 0;
    il_sea number := 0;
    il_mie number := 0;
    seanse_id number := 0;
Begin
    SELECT KLIENCI_SEQ.currval
    INTO il_kli
    FROM dual;

    SELECT SEANSE_SEQ.currval
    INTO il_sea
    FROM dual;

    Loop
    Begin
        seanse_id := round(dbms_random.value(1, il_sea));

        SELECT COUNT(*)
        INTO il_mie 
        FROM MIEJSCA 
        WHERE SAL_ID = (SELECT SAL_ID FROM seanse WHERE SEA_ID = seanse_id);

        INSERT INTO ZAMOWIENIA(
            ZAM_ID, 
            KLI_ID,
            SEA_ID,
            MIE_ID,
            ZAM_Czy_ulgowy 
        )
        VALUES(
            null,
            ROUND(dbms_random.value(1, il_kli)),
            seanse_id,
            ROUND(dbms_random.value(1, il_mie)),
            MOD(rows_inserted, 2)
        );
        rows_inserted := rows_inserted + 1;
    Exception When DUP_VAL_ON_INDEX Then Null;
    End;
    exit when rows_inserted = 50;
    End loop;
End;
/

DROP TABLE temp;
